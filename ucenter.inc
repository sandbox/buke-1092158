<?php

/**
 * * get {ucenter_users} drupal uid
 * *
 * * @return void
 * **/
function ucenter_getdrupaluid ($ucenter_uid){
  $result = db_result(db_query('SELECT * FROM {ucenter_users}  WHERE ucenter_uid = %d ', (int)$ucenter_uid));
  if (!$result){
    return FALSE;
  }  
  return $result->drupal_uid;
}



/**
 * * load {ucenter_users}
 * *
 * * @return void
 * **/
function ucenter_load ($ucenter_info = array()) {
  $query = array();
  $params = array();

  if (is_numeric($ucenter_info)) {
    $ucenter_info = array('drupal_uid' => $ucenter_info);
  }
  elseif (!is_array($ucenter_info)) {
    return FALSE;
  }
  
  foreach ($ucenter_info as $key => $value) {
    if ($key == 'drupal_uid' || $key == 'ucenter_uid' || $key == 'drupal_actived') {
      $query[] = "$key = %d";
      $params[] = $value;
    }
    else {
      $query[] = "LOWER($key) = LOWER('%s')";
      $params[] = $value;
    }
  }
  $result = db_query('SELECT * FROM {ucenter_users} ucenter WHERE ' . implode(' AND ', $query), $params);

  if ($ucenter_user = db_fetch_object($result)) {
    $ucenter_user = drupal_unpack($ucenter_user);
  }
  else {
    $ucenter_user = FALSE;
  }

  return $ucenter_user;
}

/**
 * * delete {ucenter_users} record
 * *
 * * @return void
 * **/
function ucenter_delete ($drupal_uid)  {
  return db_query('DELETE FROM {ucenter_users} WHERE drupal_uid = %d ', $drupal_uid);
}

/**
 * * insert or update {ucenter_users} record
 * *
 * * @return void
 * **/
function ucenter_save($ucenter_account, $array = array()){
  if (is_object($ucenter_account) && $ucenter_account->drupal_uid) {
    $query = '';
    foreach ($array as $key => $value) {
      if (!empty($key)) {
        if ($key == 'drupal_uid' || $key == 'ucenter_uid' || $key == 'drupal_actived') {
          $query .= "$key = %d , ";
          $v[] = $value;
        }
      }    
    }
    $success = db_query("UPDATE {ucenter_users} SET $query WHERE ucenter_uid = %d", array_merge($v, array($ucenter_account->drupal_uid)));
    if (!$success) {
      return FALSE;
    }
  }
  else{
    foreach ($array as $key => $value) {
      if ($key == 'drupal_uid' || $key == 'ucenter_uid' || $key == 'drupal_actived') {
        $fields[] = $key;
        $values[] = $value;
        $s[] = "%d";
      }
    }
    $success = db_query('INSERT INTO {ucenter_users} (' . implode(', ', $fields) . ') VALUES (' . implode(', ', $s) . ')', $values);
    if (!$success) {
      return FALSE;
    }
  }
  return TRUE;
}



/**
 *  
 *  edit /uc_client/config.inc.php
 *  @return void
 */
function ucenter_setconfig($settings_file,$settings = array()){
  
  // Build list of setting names and insert the values into the global namespace.
  $keys = array();
  foreach ($settings as $setting => $data) {
    $GLOBALS[$setting] = $data['value'];
    $keys[] = $setting;
  }

  $buffer = NULL;
  $first = TRUE;
  if ($fp = fopen($settings_file, 'r')) {
    // Step line by line through settings.php.
    while (!feof($fp)) {
      $line = fgets($fp);
      if ($first && substr($line, 0, 5) != '<?php') {
        $buffer = "<?php\n\n";
      }
      $first = FALSE;
      // Check for constants.
      if (substr($line, 0, 7) == 'define(') {
        preg_match('/define\(\s*[\'"]([A-Z_-]+)[\'"]\s*,(.*?)\);/', $line, $variable);
        if (in_array($variable[1], $keys)) {
          $setting = $settings[$variable[1]];
          if (is_string($setting['value'])){
            $buffer .= str_replace($variable[2], " '" . $setting['value'] . "'", $line);
          }
          else{
            $buffer .= str_replace($variable[2],  $setting['value'] , $line);
          }          
          
          unset($settings[$variable[1]]);
          unset($settings[$variable[2]]);
        }
        else {
          $buffer .= $line;
        }
      }
      // Check for variables.
      elseif (substr($line, 0, 1) == '$') {
        preg_match('/\$([^ ]*) /', $line, $variable);
        if (in_array($variable[1], $keys)) {
          // Write new value to settings.php in the following format:
          //    $'setting' = 'value'; // 'comment'
          $setting = $settings[$variable[1]];
          $buffer .= '$' . $variable[1] . " = '" . $setting['value'] . "';" . (!empty($setting['comment']) ? ' // ' . $setting['comment'] . "\n" : "\n");
          unset($settings[$variable[1]]);
        }
        else {
          $buffer .= $line;
        }
      }
      else {
        $buffer .= $line;
      }
    }
    fclose($fp);
    //$buffer = str_replace("\r\n","\n",$buffer);
    $fp = fopen($settings_file, 'w');
    if ($fp && fwrite($fp, $buffer) === FALSE) {
      drupal_set_message(st('Failed to modify %settings, please verify the file permissions.', array('%settings' => $settings_file)), 'error');
    }
  }
  else {
    drupal_set_message(st('Failed to open %settings, please verify the file permissions.', array('%settings' => $settings_file)), 'error');
  }
}

/**
 *  
 *  get /uc_client/config.inc.php
 *  @return void
 */
function ucenter_getconfig($settings_file,$key){
  
  if ($fp = fopen($settings_file, 'r')) {
    // Step line by line through settings.php.
    while (!feof($fp)) {
      $line = fgets($fp);
      // Check for constants.
      if (substr($line, 0, 7) == 'define(') {
        preg_match('/define\(\s*[\'"]([A-Z_-]+)[\'"]\s*,(.*?)\);/', $line, $variable);
        if ($key == $variable[1] ) {      
          return trim($variable[2]);
        }
      }
      // Check for variables.
      elseif (substr($line, 0, 1) == '$') {
        preg_match('/\$([^ ]*) /', $line, $variable);
        if ($key == $variable[1] ) {
          return trim($variable[2]);
        }
      }
    }
    fclose($fp);
    return FALSE;
  }
  else {
    drupal_set_message(st('Failed to open %settings, please verify the file permissions.', array('%settings' => $settings_file)), 'error');
    return FALSE;
  }
}





