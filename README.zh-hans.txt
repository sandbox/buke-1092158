********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
名称: Ucenter Module
作者: buke < wangbuke@gmail.com >
Drupal: 6
********************************************************************

注意: 当ucenter及其他应用(非druapl)修改密码时，ucenter并不会发起修改
密码请求（或传递的密码参数为空）给其他应用。仅当用户在drupal中登
录成功，本模块才能从ucenter中获取并保存用户密码。
      
简介:

本模块旨在整合drupal和ucenter(一个在中国非常流行的用户管理系统)。同时
可以和许多网站系统整合，比如 "ECShop" "ShopEx" "ECMall" "Discuz! X" 
"Discuz!" "UCenter Home" "SupeSite" "X-Space" "ECMS" "PHPcms" "DedeCms"
"HDwiki" "PBdigg" 等等。

本模块目前提供以下功能：
●用户同步登录
●用户同步登出
●同步修改用户资料
●同步删除用户
●同步添加用户
●支持GBK/UTF8/BIG5等编码的ucenter

********************************************************************

安装:

在drupal中配置：

1. 下载并解压ucenter 模块，将解压的文件夹放到 Drupal 的 modules 目录 (
   例如 sites/all/modules/ucenter 或 modules/ucenter).

2. 在drupal 的管理后台启用本模块:

     Administer > Site building > Modules

3. 启用本模块后，你还必须进入ucenter后台进行配置。请参考下面的章节。   

在ucenter中配置：

1. 进入ucenter后台，添加新应用：

    ucenter 用户管理中心 > 应用管理 > 添加新应用

2. 选择自定义安装：

    应用名称: (可随意填写，例如 drupal)
    应用的URL: (uc_client/api 目录名，例如 http://127.0.0.1/drupal/
    sites/all/modules/ucenter/uc_client)
    通信密钥: (可随意填写，为空时系统自动生成随机密钥)
    应用类型: (请选择其他)
    是否开启同步登录: (请选择是)
    是否接受通知：(请选择是)
    其他: (请保持默认值或为空)

3. 点击"提交"按钮，提示"成功添加应用"。复制页面下方的"应用的 Ucenter
   配置信息"。

4. 进入drupal的管理后台，找到ucenter模块的Ucenter server settings设置:

    Administer > Site configuration > Ucenter > Ucenter server settings

    清空文本框内容，将刚才复制的代码，粘贴到文本框内。点击"保存"。

5. 恭喜！你已经完成ucenter模块的基本配置。

********************************************************************
