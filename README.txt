********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Ucenter Module
Author: buke < wangbuke@gmail.com >
Drupal: 6
********************************************************************

NOTE: Ucenter do not send edit password request to other apps (or 
send empty password ) when ucenter and other apps edit user password.
This module will get and update user password when user login success
-fully from drupal.

DESCRIPTION:

This module provides integration with Ucenter (a popular PHP user 
manager system in China). And you can easy integrate many product like
"ECShop" "ShopEx" "ECMall" "Discuz! X" "Discuz!" "UCenter Home" 
"SupeSite" "X-Space" "ECMS" "PHPcms" "DedeCms" "HDwiki" "PBdigg" 
and other else.

Now this module provides:

  User sign on synchronously
  User sign out synchronously
  Edit user profile synchronously
  Delete user synchronously
  Insert user synchronously
  Support multiple encode (UTF8/GBK/BIG5) ucenter

********************************************************************

INSTALLATION:

Setup in drupal：

1.  Place the entire actions directory into your Drupal modules
   directory (sites/all/modules/ucenter or modules/ucenter).

2. Enable the ucenter module by navigating to:

     Administer > Site building > Modules

3. After the module is enabled, you should go to ucenter administer 
page and setting. See below setion.   

Setup in ucenter：

1. Got to ucenter administer page, add new apps:

    Ucenter user manager center > App manage > Add new app

2. Choose custom install:

    App Name: (Enter something you like，For example:drupal)
    APP URL: (uc_client/api dir name，For example: http://127.0.0.1/drupal/
    sites/all/modules/ucenter/uc_client)
    KEY: (Enter something you like，Leave blank would generate random
    key)
    App type: (Choose other)
    Enable sync login: (Choose enable)
    Enable recevie note：(Choose enable)
    Others: (Default value or blank)

3. Click "commit", and see success message. Copy the "Ucenter app config 
   information" textarea .

4. Got to drupal administrater page, look up ucenter module setting page
   "Ucenter server settings" :

    Administer > Site configuration > Ucenter > Ucenter server settings

    Clear the textarea, paste into textarea. Click "Save configration".

5. Congratulations! You have been finish the basic config to integrate 
   Ucenter with Drupal.

********************************************************************
