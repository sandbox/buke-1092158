<?php

/**
 * Menu callback: list all user
 */
function ucenter_admin_users() {
  $header = array(
    array(),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Drupal UID'), 'field' => 'u.uid', 'sort' => 'desc'),
    t('Ucenter UID'),
    t('Operations'),
    t('Sync status'),
  );
  $sql = 'SELECT DISTINCT u.uid, u.name, du.ucenter_uid FROM {users} u LEFT JOIN {ucenter_users} du ON u.uid = du.drupal_uid  WHERE u.uid != 0 ';
  $query_count = 'SELECT  COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {ucenter_users} du ON u.uid = du.drupal_uid  WHERE u.uid != 0 ';
 
  $sql .= tablesort_sql($header);
  $result = pager_query($sql, 50, 0, $query_count );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array (
      'sync' =>  t('Synchronise user with Ucenter'),
      'desync' =>  t('Desynchronize user with Ucenter'),
    ),
    '#default_value' => 'sync',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  
  $destination = drupal_get_destination();
  $accounts = array();
  while ($account = db_fetch_object($result)) {
    $accounts[$account->uid] = '';
    $form['name'][$account->uid] = array('#value' => theme('username', $account));
    $form['uid'][$account->uid] = array('#value' => $account->uid);
    $form['ucenter_uid'][$account->uid] = array('#value' => $account->ucenter_uid);
    $form['operations'][$account->uid] = array('#value' => l(t('edit'), "user/$account->uid/edit", array('query' => $destination)));
    $form['sync_status'][$account->uid] = array(
      '#value' => array(
        'ucenter_uid' => $account->ucenter_uid ,
        'name' => $account->name ,
      ),
      '#theme' => 'ucenter_admin_users_sync_status',
    );
  }
  $form['accounts'] = array(
    '#type' => 'checkboxes',
    '#options' => $accounts
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;


}



/**
 * Submit the user administration update form.
 */
function ucenter_admin_users_submit($form, &$form_state) {
  $operation = $form_state['values']['operation'];
  // Filter out unchecked accounts.
  $accounts = array_filter($form_state['values']['accounts']);
  if ($operation == 'desync') {
    ucenter_admin_users_operations_desync($accounts);
  }
  else if ($operation == 'sync') {
    ucenter_admin_users_operations_sync($accounts);
  }
  drupal_set_message(t('The update has been performed.'));
}



/**
 * Callback function for admin mass desync ucenter users.
 */
function ucenter_admin_users_operations_desync($accounts) {
  foreach ($accounts as $uid) {
    db_query('DELETE FROM {ucenter_users} WHERE drupal_uid = %d ',(int)$uid);
    watchdog('user', 'Ucenter: Stop sync with ucenter . drupal UID: %uid.', array('%uid' => $uid));
  }
}



/**
 * Callback function for admin mass sync ucenter users.
 */
function ucenter_admin_users_operations_sync($accounts) {
  foreach ($accounts as $uid) {
    $account = user_load(array('uid' => (int)$uid));
    $ucenter_user = ucenter_load((int)$uid); 
    if (!$ucenter_user) {
      @$re = uc_get_user($account->name);
      if ($re) {
        ucenter_save ('',array('drupal_uid' => $account->uid, 'ucenter_uid' => (int) $re[0], 'drupal_actived' => 0,));
      }
    }
  }
}



/**
 * Theme user administration overview.
 *
 * @ingroup themeable
 */
function theme_ucenter_admin_users($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Drupal UID'), 'field' => 'u.uid', 'sort' => 'desc'),
    t('Ucenter UID'),
    t('Operations'),
    t('Sync status'),
  );

  $output = drupal_render($form['options']);
  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $rows[] = array(
        drupal_render($form['accounts'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['uid'][$key]),
        drupal_render($form['ucenter_uid'][$key]),
        drupal_render($form['operations'][$key]),
        drupal_render($form['sync_status'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => t('No users available.'), 'colspan' => '7'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}




/**
 * Theme node administration filter selector.
 *
 * @ingroup themeable
 */
function theme_ucenter_admin_users_sync_status($form) {
  $output = '';
  if (empty($form['#value']['ucenter_uid'])) {
    
    try {
      @$re = uc_user_checkname($form['#value']['name']);
    } catch (Exception $e) {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-error.png').') no-repeat scroll 0px 50% transparent;">';      
      $output .= ($e->getMessage());
      $output .= '<em>'.t('Suggestion: Please check ');
      $output .= l('Ucenter Server Settings ',  'admin/user/ucenter/settings/uc_server' );
      $output .= '.<em/>';
      $output .= '</div>';  
      return $output;
    }
    
    if ($re == 1) {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-warning.png').') no-repeat scroll 0px 50% transparent;">';
      $output .= t('User did not synchronised with Ucenter. ');
      $output .= '<br/><em>'.t('Suggestion: Nothing.').'<em/>';
      $output .= '<br/><em>'.t('User will auto import to Ucenter when the user login from drupal site.').'<em/>';
      $output .= '</div>';
    }
    else if ($re == -1) {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-warning.png').') no-repeat scroll 0px 50% transparent;">';
      $output .= t('The name %name is not valid.', array('%name' =>$form['#value']['name']));
      $output .= '<br/><em>'.t('Suggestion: Change the user name before synchronise with Ucenter').'<em/>';
      $output .= '</div>';
    }
    else if ($re == -2) {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-warning.png').') no-repeat scroll 0px 50% transparent;">';      
      $output .= t('The name %name is including bad word.', array('%name' =>$form['#value']['name']));
      $output .= '<br/><em>'.t('Suggestion: Change the user name before synchronise with Ucenter').'<em/>';
      $output .= '</div>';
    }
    else if ($re == -3) {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-warning.png').') no-repeat scroll 0px 50% transparent;">';      
      $output .= t('The name %name is already taken.', array('%name' =>$form['#value']['name']));
      $output .= '<br/><em>'.t('Suggestion: Change the user name before synchronise with Ucenter').'<em/>';
      $output .= '</div>';
    }
    else {
      $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-error.png').') no-repeat scroll 0px 50% transparent;">';      
      $output .= t('Unknow error.');
      $output .= '</div>';
    }
  }
  else {
    $output .= '<div style="padding-left: 30px;background: url('.url('misc/watchdog-ok.png').') no-repeat scroll 0px 50% transparent;">';
    $output .= t('Synchronised !');
    $output .= '</div>';
  }  
  return $output;
}






/**
 * Menu callback: setting /api/uc.php 
 */
function ucenter_admin_uc_client() {
  if (!user_access('access administration pages')){
    return message_access();
  }

  
	$form['uc_client_settings'] = array('#type' => 'fieldset',
        '#title' => t('Ucenter client settings'),
        '#description' => t('This is Ucenter client settings. You can edit the file(%file) manually on your risk.' ,array('%file' => drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php',)),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
      );
	$form['uc_client_settings']['API_DELETEUSER'] = array('#type' => 'checkbox',
        '#title' => t('API_DELETEUSER'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_DELETEUSER'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_RENAMEUSER'] = array('#type' => 'checkbox',
        '#title' => t('API_RENAMEUSER'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_RENAMEUSER'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_GETTAG'] = array('#type' => 'checkbox',
        '#title' => t('API_GETTAG'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_GETTAG'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );
	$form['uc_client_settings']['API_SYNLOGIN'] = array('#type' => 'checkbox',
        '#title' => t('API_SYNLOGIN'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_SYNLOGIN'),
        '#required' => TRUE,
      );
	$form['uc_client_settings']['API_SYNLOGOUT'] = array('#type' => 'checkbox',
        '#title' => t('API_SYNLOGOUT'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_SYNLOGOUT'),
        '#required' => TRUE,
      );
	$form['uc_client_settings']['API_UPDATEPW'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATEPW'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATEPW'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_UPDATEBADWORDS'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATEBADWORDS'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATEBADWORDS'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );
  $form['uc_client_settings']['API_UPDATEHOSTS'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATEHOSTS'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATEHOSTS'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_UPDATEAPPS'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATEAPPS'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATEAPPS'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_UPDATECLIENT'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATECLIENT'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATECLIENT'),
        '#required' => TRUE,
      );
  $form['uc_client_settings']['API_UPDATECREDIT'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATECREDIT'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATECREDIT'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );
  $form['uc_client_settings']['API_GETCREDITSETTINGS'] = array('#type' => 'checkbox',
        '#title' => t('API_GETCREDITSETTINGS'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_GETCREDITSETTINGS'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );
  $form['uc_client_settings']['API_GETCREDIT'] = array('#type' => 'checkbox',
        '#title' => t('API_GETCREDIT'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_GETCREDIT'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );
  $form['uc_client_settings']['API_UPDATECREDITSETTINGS'] = array('#type' => 'checkbox',
        '#title' => t('API_UPDATECREDITSETTINGS'), 
        '#default_value' => ucenter_getconfig(drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php','API_UPDATECREDITSETTINGS'),
        '#required' => TRUE,
        '#disabled' => FALSE,
      );

  

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['#submit'][] = 'ucenter_admin_uc_client_submit';
  return $form;
 
}


function ucenter_admin_uc_client_submit($form, &$form_state) {
  $uc_php_settings['API_DELETEUSER'] = array('value' => $form_state['values']['API_DELETEUSER'], );
  $uc_php_settings['API_RENAMEUSER'] = array('value' => $form_state['values']['API_RENAMEUSER'], );
  $uc_php_settings['API_GETTAG'] = array('value' => $form_state['values']['API_GETTAG'], );
  $uc_php_settings['API_SYNLOGIN'] = array('value' => $form_state['values']['API_SYNLOGIN'], );
  $uc_php_settings['API_SYNLOGOUT'] = array('value' => $form_state['values']['API_SYNLOGOUT'], );
  $uc_php_settings['API_UPDATEPW'] = array('value' => $form_state['values']['API_UPDATEPW'], );
  $uc_php_settings['API_UPDATEBADWORDS'] = array('value' => $form_state['values']['API_UPDATEBADWORDS'], );
  $uc_php_settings['API_UPDATEHOSTS'] = array('value' => $form_state['values']['API_UPDATEHOSTS'], );
  $uc_php_settings['API_UPDATEAPPS'] = array('value' => $form_state['values']['API_UPDATEAPPS'], );
  $uc_php_settings['API_UPDATECLIENT'] = array('value' => $form_state['values']['API_UPDATECLIENT'], );
  $uc_php_settings['API_UPDATECREDIT'] = array('value' => $form_state['values']['API_UPDATECREDIT'], );
  $uc_php_settings['API_GETCREDITSETTINGS'] = array('value' => $form_state['values']['API_GETCREDITSETTINGS'], );
  $uc_php_settings['API_GETCREDIT'] = array('value' => $form_state['values']['API_GETCREDIT'], );
  $uc_php_settings['API_UPDATECREDITSETTINGS'] = array('value' => $form_state['values']['API_UPDATECREDITSETTINGS'], );

  ucenter_setconfig (drupal_get_path('module', 'ucenter').'/uc_client/api/uc.php',$uc_php_settings); 
  drupal_set_message(t('The configuration options have been saved.'));
}



/**
 * Menu callback: setting conf.inc.php 
 */
function ucenter_admin_uc_server() {
  if (!user_access('access administration pages')){
    return message_access();
  }

  $buffer = NULL; 
  if ($fp = fopen(drupal_get_path('module', 'ucenter').'/uc_client/config.inc.php', 'r')) {
    while (!feof($fp)) {
      $line = fgets($fp);
      if ( substr(trim($line), 0, 5) != '<?php') {
        $buffer .= $line;
      }
    }
  }
  fclose($fp);  
	$form['uc_server_settings'] = array('#type' => 'fieldset',
        '#title' => t('Ucenter server settings'),
        '#description' => t('This is Ucenter server settings. You can edit the file(%file) manually on your risk.' ,array('%file' => drupal_get_path('module', 'ucenter').'/uc_client/config.inc.php',)),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
      );
	$form['uc_server_settings']['file_body'] = array('#type' => 'textarea',
        '#default_value' => $buffer,
        '#required' => TRUE,
        '#rows' => 15,
      );
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['test_conf'] = array('#type' => 'submit', '#value' => t('Test configuration') );
  $form['#submit'][] = 'ucenter_admin_uc_server_submit';
  return $form;

}

function ucenter_admin_uc_server_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save configuration')) {
    $buffer = "<?php\n\n".$form_state['values']['file_body'];
    $buffer = str_replace("\r\n","\n",$buffer);
    $fp = fopen(drupal_get_path('module', 'ucenter').'/uc_client/config.inc.php', 'w'); 
    if ($fp && fwrite($fp, $buffer) === FALSE) {
        drupal_set_message(st('Failed to modify %settings, please verify the file permissions.', array('%settings' => drupal_get_path('module', 'ucenter').'/uc_client/config.inc.php')), 'error');
    }
    fclose($fp);   
    drupal_set_message(t('The configuration options have been saved.'));
  }
  else {
    try {
      $re = uc_user_checkname('');
      drupal_set_message(t('Test configuration OK !'));
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(),'error');
      drupal_set_message(t('Please check again. Are you forgot click "Save configuration" before click "Test configuration" ?'),'error');
    }
  }
}



//ucenter_admin_ucenter
/**
 * Menu callback: setting conf.inc.php 
 */
function ucenter_admin_ucenter_settings() {
  if (!user_access('access administration pages')){
    return message_access();
  }

  $form['ucenter_settings'] = array('#type' => 'fieldset',
        '#title' => t('Drupal user action settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
      );
  $form['ucenter_settings']['ucenter_loginuc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Login Ucenter user on drupal site user just logged in'), 
        '#default_value' => variable_get('ucenter_loginuc',TRUE), 
        '#required' => True,
      );
  $form['ucenter_settings']['ucenter_logoutuc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Logout Ucenter user on drupal site user just logged out'), 
        '#default_value' => variable_get('ucenter_logoutuc',TRUE), 
        '#required' => True,
      );
  $form['ucenter_settings']['ucenter_deleteuc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete Ucenter user on drupal site user is being deleted'), 
        '#default_value' => variable_get('ucenter_deleteuc',TRUE), 
        '#required' => True,
      );
  $form['ucenter_settings']['ucenter_updateuc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Update Ucenter user on drupal site user is being changed'), 
        '#default_value' => variable_get('ucenter_updateuc',TRUE), 
        '#required' => True,
      );
  $form['ucenter_settings']['ucenter_insertuc'] = array(
        '#type' => 'checkbox',
        '#title' => t('Insert Ucenter user on drupal site user is being added'), 
        '#default_value' => variable_get('ucenter_insertuc',TRUE), 
        '#required' => True,
      );

  
  $form['ucenter_autocreat'] = array('#type' => 'fieldset',
        '#title' => t('Auto creat user setting'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
      );
  $form['ucenter_autocreat']['ucenter_auto_uc_auth'] = array(
        '#type' => 'checkbox',
        '#title' => t('Auto create user to drupal as user has been authenticate by Ucenter'), 
        '#default_value' => variable_get('ucenter_auto_uc_auth',TRUE), 
        '#required' => True,
      );
  $form['ucenter_autocreat']['ucenter_create_even_same'] = array(
        '#type' => 'checkbox',
        '#title' => t('Create user to drupal even user name is same with Ucenter.'), 
        '#default_value' => variable_get('ucenter_create_even_same',FALSE), 
        '#required' => True,
      );

  return system_settings_form($form);

}


